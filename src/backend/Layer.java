/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;

import static java.lang.Math.exp;
import static java.lang.Math.sqrt;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;

/**
 *
 * @author Panda
 */
public class Layer {

    private final Neuron[][] neurons;
    private final int neighborhoodFunctionType;
    private final int numberOfNeurons;
    private final int numberOfInputs;
    private final int numberOfRecords;
    private final int distanceMetric;
    private final double alpha;
    private double lambda;
    private final double[][] uMatrix;
    private final int[][] scaledUMatrix;

    public Layer(int numberOfNeurons, int numberOfInputs, int numberOfRecords, double alpha, double lambda, int neighborhood, int metric) {
        this.numberOfRecords = numberOfRecords;
        this.numberOfNeurons = numberOfNeurons;
        this.numberOfInputs = numberOfInputs;
        this.alpha = alpha;
        this.lambda = lambda;
        this.distanceMetric = metric;
        this.neighborhoodFunctionType = neighborhood;
        this.uMatrix = new double[2 * numberOfNeurons - 1][2 * numberOfNeurons - 1];
        this.scaledUMatrix = new int[2 * numberOfNeurons - 1][2 * numberOfNeurons - 1];
        neurons = new Neuron[numberOfNeurons][numberOfNeurons];
        for (int i = 0; i < numberOfNeurons; i++) {
            for (int j = 0; j < numberOfNeurons; j++) {
                neurons[i][j] = new Neuron(numberOfInputs);
                neurons[i][j].setDistances(new double[numberOfNeurons][numberOfNeurons]);
            }
        }
        this.computeDistances();

    }

    private double gaussNeighborhood(int i, int j, int k, int l) {
        double a1 = -(this.neurons[i][j].getDistance()[k][l]) * (this.neurons[i][j].getDistance()[k][l]);
        double result = exp(a1 / (2 * (lambda * lambda)));
        return result;

    }

    public void learn(int epochs) {
        RandList los;
        int bestI = 0;
        int bestJ = 0;
        double pmin = 0.75;
        int current = 0;

        double dist = 0;
        double tempDist = 0;
        double bestDist = 999999999;
        double startLambda = this.lambda;
        double difference = 0;
        for (int e = 0; e < epochs; e++) {
            los = new RandList(this.numberOfRecords);
            for (int n = 0; n < this.numberOfRecords; n++) {
                current = los.get();  //random vector from learning set

                //looking for BMU
                for (int k = 0; k < this.numberOfNeurons; k++) {
                    for (int l = 0; l < this.numberOfNeurons; l++) {
                        if (this.neurons[k][l].getConscience() > pmin) { //check if neuron is eligible for competition 
                            dist = 0;
                            switch (this.distanceMetric) {
                                case MetricType.EUCLIDEAN_METRIC:
                                    dist = euclideanDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                                    break;
                                case MetricType.SCALAR_PRODUCT_METRIC:
                                    dist = scalarProductDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                                    break;
                                case MetricType.L1_MANHATTAN_METRIC:
                                    dist = l1ManhattanDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                                    break;
                                case MetricType.MAX_DIFFERENCE_METRIC:
                                    dist = maxDifferenceDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                                    break;
                            }
                            if (dist < bestDist) {
                                bestDist = dist;
                                bestI = k;
                                bestJ = l;

                            }
                        }

                    }
                }
                this.neurons[bestI][bestJ].setConscience(this.neurons[bestI][bestJ].getConscience() - pmin);

                //weights adaptation
                for (int i = 0; i < numberOfNeurons; i++) {
                    for (int j = 0; j < numberOfNeurons; j++) {
                        double a = this.neurons[bestI][bestJ].getDistance()[i][j];
                        if (a < this.lambda) {
                            switch (this.neighborhoodFunctionType) {
                                case NeighborhoodType.GAUSSIAN:
                                    for (int k = 0; k < numberOfInputs; k++) {
                                        this.neurons[i][j].setWeight(this.neurons[i][j].getWeights()[k] + alpha * gaussNeighborhood(bestI, bestJ, i, j) * (DataUtils.records[current][k] - this.neurons[i][j].getWeights()[k]), k);
                                    }
                                    break;
                                case NeighborhoodType.SQUARE:
                                    for (int k = 0; k < numberOfInputs; k++) {
                                        this.neurons[i][j].setWeight(this.neurons[i][j].getWeights()[k] + alpha * (DataUtils.records[current][k] - this.neurons[i][j].getWeights()[k]), k);
                                    }
                                    break;
                            }
                        }
                        if (this.neurons[i][j].getConscience() < 1 && i != bestI && j != bestJ) {
                            this.neurons[i][j].setConscience(this.neurons[i][j].getConscience() + 1.0/(this.numberOfNeurons*this.numberOfNeurons));
                        }
                    }
                }
                bestI = 0;
                bestJ = 0;
                bestDist = 999999;

                this.lambda = startLambda - (((double) e + 1.0) / (double) epochs) * (startLambda - 2);

            }
        }
    }

    public void calibrate() {
        double bestDist;
        double dist;
        for (int i = 0; i < this.numberOfNeurons; i++) {
            for (int j = 0; j < this.numberOfNeurons; j++) {
                bestDist = 99999;
                for (int k = 0; k < this.numberOfRecords; k++) {
                    dist = 0;
                    for (int l = 0; l < this.numberOfInputs; l++) {
                        dist += ((this.neurons[i][j].getWeights()[l] - DataUtils.records[k][l]) * (this.neurons[i][j].getWeights()[l] - DataUtils.records[k][l]));
                    }
                    dist = sqrt(dist);
                    if (dist < bestDist) {
                        bestDist = dist;
                        this.neurons[i][j].setType(DataUtils.results[k]);
                    }
                }

            }

        }
    }

    public void calibrate2() {
        double bestDist = 999999;
        double dist, tempDist, difference;
        int bestI = 0, bestJ = 0;
        for (int current = 0; current < this.numberOfRecords; current=current+1) {
            for (int k = 0; k < this.numberOfNeurons; k++) {
                for (int l = 0; l < this.numberOfNeurons; l++) {
                    dist = 0;
                    dist = 0;
                    switch (this.distanceMetric) {
                        case MetricType.EUCLIDEAN_METRIC:
                            dist = euclideanDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                            break;
                        case MetricType.SCALAR_PRODUCT_METRIC:
                            dist = scalarProductDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                            break;
                        case MetricType.L1_MANHATTAN_METRIC:
                            dist = l1ManhattanDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                            break;
                        case MetricType.MAX_DIFFERENCE_METRIC:
                            dist = maxDifferenceDistance(DataUtils.records[current], this.neurons[k][l].getWeights());
                            break;
                    }
                    if (dist < bestDist) {
                        bestDist = dist;
                        bestI = k;
                        bestJ = l;

                    }
                }

            }
            if( this.neurons[bestI][bestJ].getType().equals("---"))
            {
            this.neurons[bestI][bestJ].setType(DataUtils.results[current]);
            }
            else
            {
                this.neurons[bestI][bestJ].setType(this.neurons[bestI][bestJ].getType()+"\n"+(DataUtils.results[current]));
            }
            bestI = 0;
            bestJ = 0;
            bestDist = 999999;
        }
    }

    private void computeDistances() {
        for (int i = 0; i < this.numberOfNeurons; i++) {
            {
                for (int j = 0; j < this.numberOfNeurons; j++) {
                    for (int k = 0; k < this.numberOfNeurons; k++) {
                        for (int l = 0; l < this.numberOfNeurons; l++) {
                            this.neurons[i][j].setDistance(sqrt((i - k) * (i - k) + (j - l) * (j - l)), k, l);
                        }
                    }

                }

            }
        }
    }

    public void computeUMatrix() {
        double temp = 0;
        int neighborCounter = 0;
        for (int i = 0; i < this.numberOfNeurons; i++) {
            for (int j = 0; j < this.numberOfNeurons; j++) {
                switch (this.distanceMetric) {
                    case MetricType.EUCLIDEAN_METRIC:
                        if (j < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2][j * 2 + 1] = euclideanDistance(this.neurons[i][j].getWeights(), this.neurons[i][j + 1].getWeights());
                        }
                        if (i < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2 + 1][j * 2] = euclideanDistance(this.neurons[i][j].getWeights(), this.neurons[i + 1][j].getWeights());
                        }
                        break;
                    case MetricType.L1_MANHATTAN_METRIC:
                        if (j < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2][j * 2 + 1] = l1ManhattanDistance(this.neurons[i][j].getWeights(), this.neurons[i][j + 1].getWeights());
                        }
                        if (i < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2 + 1][j * 2] = l1ManhattanDistance(this.neurons[i][j].getWeights(), this.neurons[i + 1][j].getWeights());
                        }
                        break;
                    case MetricType.MAX_DIFFERENCE_METRIC:
                        if (j < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2][j * 2 + 1] = maxDifferenceDistance(this.neurons[i][j].getWeights(), this.neurons[i][j + 1].getWeights());
                        }
                        if (i < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2 + 1][j * 2] = maxDifferenceDistance(this.neurons[i][j].getWeights(), this.neurons[i + 1][j].getWeights());
                        }
                        break;
                    case MetricType.SCALAR_PRODUCT_METRIC:
                        if (j < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2][j * 2 + 1] = scalarProductDistance(this.neurons[i][j].getWeights(), this.neurons[i][j + 1].getWeights());
                        }
                        if (i < this.numberOfNeurons - 1) {
                            this.uMatrix[i * 2 + 1][j * 2] = scalarProductDistance(this.neurons[i][j].getWeights(), this.neurons[i + 1][j].getWeights());
                        }
                        break;
                }
            }

        }
        for (int i = 0; i < this.numberOfNeurons * 2 - 1; i++) {
            for (int j = 0; j < this.numberOfNeurons * 2 - 1; j++) {
                if (this.uMatrix[i][j] == 0) {
                    if (i > 0) {
                        this.uMatrix[i][j] += this.uMatrix[i - 1][j];
                        neighborCounter++;
                    }
                    if (i < this.uMatrix.length - 1) {
                        this.uMatrix[i][j] += this.uMatrix[i + 1][j];
                        neighborCounter++;
                    }
                    if (j > 0) {
                        this.uMatrix[i][j] += this.uMatrix[i][j - 1];
                        neighborCounter++;
                    }
                    if (j < this.uMatrix.length - 1) {
                        this.uMatrix[i][j] += this.uMatrix[i][j + 1];
                        neighborCounter++;
                    }
                    this.uMatrix[i][j] = this.uMatrix[i][j] / neighborCounter;
                    neighborCounter = 0;
                }

            }
        }

    }

    double euclideanDistance(double[] vec1, double[] vec2) {
        double dist = 0;
        for (int j = 0; j < this.numberOfInputs; j++) {
            dist = dist + (vec1[j] - vec2[j]) * (vec1[j] - vec2[j]);
        }
        dist = sqrt(dist);
        return dist;
    }

    double l1ManhattanDistance(double[] vec1, double[] vec2) {
        double dist = 0;
        for (int j = 0; j < this.numberOfInputs; j++) {
            dist = dist + sqrt((vec1[j] - vec2[j]) * (vec1[j] - vec2[j]));
        }
        dist = sqrt(dist);
        return dist;
    }

    double maxDifferenceDistance(double[] vec1, double[] vec2) {
        double dist = 0;
        double difference = 0;
        double tempDist = 0;
        for (int j = 0; j < this.numberOfInputs; j++) {

            difference = sqrt((vec1[j] - vec2[j]) * (vec1[j] - vec2[j]));

            if (difference > tempDist) {
                tempDist = difference;
            }
        }
        dist = tempDist;
        return dist;
    }

    double scalarProductDistance(double[] vec1, double[] vec2) {
        double dist = 0;
        for (int j = 0; j < this.numberOfInputs; j++) {
            dist = dist + (vec1[j] * vec2[j]);
        }
        return dist;
    }

    public void scaleUMatrix() {

        double max = Arrays.stream(this.uMatrix).flatMapToDouble(x -> Arrays.stream(x)).max().getAsDouble();
        double scala = 255 / max;
        for (int i = 0; i < this.scaledUMatrix.length; i++) {

            for (int j = 0; j < this.scaledUMatrix[0].length; j++) {
                this.scaledUMatrix[i][j] = (int) (this.uMatrix[i][j] * scala);

            }
        }

    }

    public Neuron[][] getNeurons() {
        return neurons;
    }

    public double[][] getuMatrix() {
        return uMatrix;
    }

    public int[][] getScaledUMatrix() {
        return scaledUMatrix;
    }

}
