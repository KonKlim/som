/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;


import java.util.Random;


/**
 *
 * @author Panda
 */
public class Neuron {

    private double[] weights;
    private double[][] distance;
    private double conscience;
    private String type;

    public Neuron(int n) {
        this.conscience = 1;
        this.type = "";
        this.weights = new double[n];
this.type="---";
        this.RandomizeWeights(n);

    }

    private void RandomizeWeights(int n) {
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            weights[i] = rand.nextDouble();
        }
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeight(double weight, int index) {
        this.weights[index] = weight;
    }

    public double[][] getDistance() {
        return distance;
    }

    public void setDistances(double[][] distance) {
        this.distance = distance;
    }
    
        public void setDistance(double distance, int i, int j) {
        this.distance[i][j] = distance;
    }

    public double getConscience() {
        return conscience;
    }

    public void setConscience(double conscience) {
        this.conscience = conscience;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
