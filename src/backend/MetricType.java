/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;

/**
 *
 * @author Panda
 */
public class MetricType 
{
	public static final int EUCLIDEAN_METRIC = 1;
	public static final int SCALAR_PRODUCT_METRIC = 2;
	public static final int L1_MANHATTAN_METRIC = 3;
	public static final int MAX_DIFFERENCE_METRIC = 4;
}
