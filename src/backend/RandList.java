package backend;

import java.util.LinkedList;
import java.util.List;

class RandList {

    private List<Integer> numbers;
    private Integer size;

    public void init() {
        numbers = new LinkedList<Integer>();
        for (int i = 0; i < this.getSize(); i++) {
            numbers.add(new Integer(i));
        }
    }

    public Integer get() {
        Integer i = (int) (Math.random() * size);
        i = numbers.get(i);
        numbers.remove(i);
        this.size = numbers.size();
        return i;
    }

    private Integer getSize() {
        return size;
    }

    public RandList(Integer size) {
        this.size = size;
        init();
    }

}
