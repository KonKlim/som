/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontend;

import backend.Neuron;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Panda
 */
public class UMatFrame extends JFrame implements ActionListener {

    private JButton[][] tab;
    private int numberOfNeurons;
    private Neuron[][] neurons;
    private double[][] uMat;

    public UMatFrame(int numberOfNeurons, int[][] colors, Neuron[][] neurons, double[][] uMat) {
        super("UMatrix");
               setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new GridLayout(numberOfNeurons * 2 - 1, numberOfNeurons * 2 - 1));
        //    setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setSize(600, 600);
        this.uMat = uMat;
        this.numberOfNeurons = numberOfNeurons;
        this.neurons = neurons;
        tab = new JButton[numberOfNeurons * 2 - 1][numberOfNeurons * 2 - 1];
        int counter = 0, counter2 = 0;
        for (int i = 0; i < numberOfNeurons * 2 - 1; i++) {
            for (int j = 0; j < numberOfNeurons * 2 - 1; j++) {

                add(tab[i][j] = new JButton());

                tab[i][j].addActionListener(this);
                if (i % 2 == 0 && j % 2 == 0) {
                    tab[i][j].setName("n" + counter2);
                    counter2++;
                }
                else
                tab[i][j].setName("u"+counter);
                counter++;

            }
        }
        for (int i = 0; i < numberOfNeurons * 2 - 1; i++) {
            for (int j = 0; j < numberOfNeurons * 2 - 1; j++) {

                tab[i][j].setBackground(new Color(255 - colors[i][j], 255 - colors[i][j], 255 - colors[i][j]));
                tab[i][j].setForeground(new Color(255, 0, 0));
            }
        }
        for (int i = 0; i < numberOfNeurons; i++) {
            for (int j = 0; j < numberOfNeurons; j++) {

                tab[i * 2][j * 2].setText("<html>" + neurons[i][j].getType().replaceAll("\n", "<br>") + "</html>");

            }
        }

        this.repaint();
        setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
        JButton btn = (JButton) evt.getSource();
        StringBuilder builder = new StringBuilder(btn.getName());
        builder.deleteCharAt(0);
        int index = Integer.parseInt(builder.toString());
        if (btn.getName().charAt(0)=='n')
        {
            new NeuronFrame(this.neurons[index/this.numberOfNeurons][index%this.numberOfNeurons].getWeights(),index/this.numberOfNeurons,index%this.numberOfNeurons);
        }
        

    }
}
