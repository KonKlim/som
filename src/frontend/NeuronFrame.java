/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontend;

import backend.Neuron;
import java.awt.BorderLayout;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

/**
 *
 * @author Panda
 */
public class NeuronFrame extends JFrame {
    
    
     public NeuronFrame(double[] weights, int i, int j) {
    super("Neuron [" + j + "][" + i + "]" );
       setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.setSize(1000,175);
    
    
    
    
 
    Vector<String> columnNames = new Vector<>();
    Vector<String> data = new Vector<>();
    Vector<Vector> rowData = new Vector<Vector>();
    
    for(int k=0;k<weights.length;k++){
        
        columnNames.addElement("Weight " + k);
       data.addElement( String.valueOf(weights[k]) );
        
}
 
    rowData.addElement(data);
    JTable table=new JTable(rowData,columnNames);
    table.setRowHeight(100);
    JScrollPane scrollPane = new JScrollPane(table);
    this.add(scrollPane, BorderLayout.CENTER);
    this.setVisible(true);
}
}
